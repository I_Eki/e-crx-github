const authorNode = document.querySelector('.author')
const projNode = document.querySelector('.author ~ [itemprop="name"]')

const paths = window.location.pathname.replace(/^\/+/, '').split('/')

if (paths.length === 2 && paths[0] === authorNode?.innerText && paths[1] === projNode?.innerText) {
  const autoItem = document.querySelector('.file-navigation > .flex-auto')

  if (autoItem) {
    const btn1s = document.createElement('summary')
    btn1s.className = 'btn-primary btn'
    btn1s.innerText = 'GitHub1s'
    btn1s.addEventListener('click', e => {
      const jumpURL = window.location.href.replace(/(?<=^http.*:\/\/)github(?=\.com)/, 'github1s')
      window.open(jumpURL)
    })

    autoItem.parentNode.insertBefore(btn1s, autoItem.nextSibling)
  }
}
